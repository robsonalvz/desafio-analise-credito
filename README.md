# Desafio Conductor - (Análise de Crédito)

Author - Robson Alves 
O projeto análise de crédito ofere a você uma forma de realizar :

* Login com diferentes profiles (Usuário, Analista)
* Autenticação JWT
* Visualização de todas as rotas do projeto através do Swagger
* Cadastro de novos portadores de cartão
* Filtros para pesquisa de portadores
* Fácil e rápida análise dos portadores


## Table of content
- [Requirements](#requirements)
- [Built With](#built-with)
- [Current Contributors](#current-contributors)

### Requirements

Para rodar a aplicação você irá precisar:

- [Java JDK 12](https://www.oracle.com/technetwork/java/javase/downloads/jdk12-downloads-5295953.html)
- [PostgreSQL 11.5](https://www.enterprisedb.com/downloads/postgres-postgresql-downloads)

## Feito com
* [Spring Boot](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/) - Web Framework
* [PostgreSQL](https://www.postgresql.org/) - Database Server 
* [Maven](https://maven.apache.org/) - Dependency Management
* [Java](http://www.oracle.com/technetwork/java/index.html) - Programming Language

#Como rodar
* Crie seu próprio application.properties com base no arquivo application-dev-properties.examples renomeando todas as variáveis de ambiente.


## Repositório do FrontEnd (Angular 8)
# https://gitlab.com/robsonalvz/analise-credito-front

Todo o processo de rodar o front-end está no README.md do repositório