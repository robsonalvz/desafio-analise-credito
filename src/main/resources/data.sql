INSERT INTO public.usuario (id, data_atualizacao, data_criacao, email, perfil, senha) VALUES (1, NOW(), NOW(), 'robson@gmail.com', 'ROLE_ANALISTA','$2a$10$WwnEzg0ppWn1q1qBwgnMEe5PN5m.a00VKrwtg2HRvBrcirJPK90b2' ) ON CONFLICT DO NOTHING;;
INSERT INTO public.usuario (id, data_atualizacao, data_criacao, email, perfil, senha) VALUES (2, NOW(), NOW(), 'joao@gmail.com', 'ROLE_USUARIO','$2a$10$WwnEzg0ppWn1q1qBwgnMEe5PN5m.a00VKrwtg2HRvBrcirJPK90b2' ) ON CONFLICT DO NOTHING;;


INSERT INTO public.portador (id, cpf, email, estado, estado_civil, idade, nome, renda, sexo,data_atualizacao, data_criacao) 
    VALUES (2, '67579799006', 'joao@gmail.com', 'BA', 'SOLTEIRO', 23, 'João Alves', 120, 'MASCULINO ',NOW(), NOW() ) ON CONFLICT DO NOTHING;

INSERT INTO public.portador (id, cpf, email, estado, estado_civil, idade, nome, renda, sexo,data_atualizacao, data_criacao) 
    VALUES (4, '56922302087', 'maria@gmail.com', 'BA', 'SOLTEIRO', 19, 'Maria Alves', 120, 'MASCULINO ',NOW(), NOW() ) ON CONFLICT DO NOTHING;

INSERT INTO public.portador (id, cpf, email, estado, estado_civil, idade, nome, renda, sexo,data_atualizacao, data_criacao) 
    VALUES (5, '46392610049', 'robson@gmail.com', 'BA', 'SOLTEIRO', 30, 'Robson Alves', 120, 'MASCULINO ',NOW(), NOW() ) ON CONFLICT DO NOTHING;

INSERT INTO public.portador (id, cpf, email, estado, estado_civil, idade, nome, renda, sexo,data_atualizacao, data_criacao) 
    VALUES (6, '02033873099', 'camila@gmail.com', 'BA', 'SOLTEIRO', 50, 'Camila Alves', 120, 'FEMININO ',NOW(), NOW() ) ON CONFLICT DO NOTHING;

INSERT INTO public.portador (id, cpf, email, estado, estado_civil, idade, nome, renda, sexo,data_atualizacao, data_criacao) 
    VALUES (7, '40124098088', 'joana@gmail.com', 'BA', 'SOLTEIRO', 19, 'Joana Alves', 120, 'FEMININO ',NOW(), NOW() ) ON CONFLICT DO NOTHING;



INSERT INTO public.analise(id, data_atualizacao, data_criacao, status, portador_id)
    VALUES (8, NOW(), NOW(), 'PENDENTE', 2) ON CONFLICT DO NOTHING;

INSERT INTO public.analise(id, data_atualizacao, data_criacao, status, portador_id)
    VALUES (9, NOW(), NOW(), 'PENDENTE', 5) ON CONFLICT DO NOTHING;

INSERT INTO public.analise(id, data_atualizacao, data_criacao, status, portador_id)
    VALUES (10, NOW(), NOW(), 'PENDENTE', 7) ON CONFLICT DO NOTHING;

INSERT INTO public.analise(id, data_atualizacao, data_criacao, status, portador_id)
    VALUES (11, NOW(), NOW(), 'PENDENTE', 4) ON CONFLICT DO NOTHING;