package com.desafio.api.security.dto;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import lombok.Data;
@Data
public class JwtAuthenticationDto {
	
	@NotEmpty(message = "Email não pode ser vazio.")
	@Email(message = "Email inválido.")
	private String email;
	
	@NotEmpty(message = "Senha não pode ser vazia.")
	private String senha;

	public JwtAuthenticationDto() {
	}


	@Override
	public String toString() {
		return "JwtAuthenticationRequestDto [email=" + email + ", password=" + senha + "]";
	}

}
