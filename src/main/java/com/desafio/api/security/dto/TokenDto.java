package com.desafio.api.security.dto;

import lombok.Data;

@Data
public class TokenDto {

	private String token; 
	private String profile;
	
	public TokenDto() {
	}
	
	public TokenDto(String token, String profile) {
		this.token = token;
		this.profile = profile;
	}
	public TokenDto(String token) {
		this.token = token;
	}

}
