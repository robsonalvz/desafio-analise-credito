package com.desafio.api.services;

import java.util.List;
import java.util.Optional;

import com.desafio.api.entities.Portador;

public interface PortadorService {
	/**
     * Persiste um Portador na base de dados.
     *
     * @param portador
     * @return Portador
     */
	Portador save(Portador portador);


    /**
     * Busca e retorna um Portador dado um email.
     *
     * @param email
     * @return Optional<Portador>
     */
    Optional<Portador> findByEmail(String email);
    
    /**
     * 
     * @param cpf
     * @return
     */
    Optional<Portador> findByCpf(String cpf);

    /**
     * Busca e retorna um Portador por ID.
     *
     * @param id
     * @return Optional<Portador>
     */
    Optional<Portador> findbyId(Long id);
    
    
    List<Portador> findAll();
}
