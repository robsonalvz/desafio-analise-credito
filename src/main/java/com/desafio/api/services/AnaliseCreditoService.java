package com.desafio.api.services;

import java.util.List;
import java.util.Optional;

import com.desafio.api.entities.AnaliseCredito;

public interface AnaliseCreditoService {
	/**
	 * 
	 * @param analiseCredito
	 * @return
	 */
	AnaliseCredito save(AnaliseCredito analiseCredito);
	/**
	 * 
	 * @param id
	 * @return
	 */
    Optional<AnaliseCredito> findbyId(Long id);
    
    /**
     * 
     * @return
     */
    List<AnaliseCredito> findAll();
}
