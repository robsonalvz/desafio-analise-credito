package com.desafio.api.services;

import java.util.Optional;

import com.desafio.api.entities.Usuario;

public interface UsuarioService {
    /**
     * Persiste um Usuario na base de dados.
     *
     * @param usuario
     * @return Usuario
     */
    Usuario save(Usuario usuario);


    /**
     * Busca e retorna um Usuario dado um email.
     *
     * @param email
     * @return Optional<Usuario>
     */
    Optional<Usuario> findByEmail(String email);

    /**
     * Busca e retorna um Usuario por ID.
     *
     * @param id
     * @return Optional<Usuario>
     */
    Optional<Usuario> findById(Long id);
}
