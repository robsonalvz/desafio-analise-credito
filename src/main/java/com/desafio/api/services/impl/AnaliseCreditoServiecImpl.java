package com.desafio.api.services.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.desafio.api.entities.AnaliseCredito;
import com.desafio.api.repositories.AnaliseCreditoRepository;
import com.desafio.api.services.AnaliseCreditoService;
@Service
public class AnaliseCreditoServiecImpl implements AnaliseCreditoService{
	
	private static final Logger log = LoggerFactory.getLogger(UsuarioServiceImpl.class);

    @Autowired
    private AnaliseCreditoRepository analiseCreditoRepository;
	@Override
	public AnaliseCredito save(AnaliseCredito analiseCredito) {
		return this.analiseCreditoRepository.save(analiseCredito);
	}

	@Override
	public Optional<AnaliseCredito> findbyId(Long id) {
		log.info("Buscando analise de credito pelo id {}", id);
		return Optional.ofNullable(this.analiseCreditoRepository.findOne(id));
	}

	@Override
	public List<AnaliseCredito> findAll() {
		return this.analiseCreditoRepository.findAll();
	}

}
