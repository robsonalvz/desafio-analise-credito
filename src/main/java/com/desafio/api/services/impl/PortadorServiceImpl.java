package com.desafio.api.services.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.desafio.api.entities.Portador;
import com.desafio.api.repositories.PortadorRepository;
import com.desafio.api.services.PortadorService;


@Service
public class PortadorServiceImpl implements PortadorService{
	
    private static final Logger log = LoggerFactory.getLogger(UsuarioServiceImpl.class);

    @Autowired
    private PortadorRepository portadorRepository;
    
	@Override
	public Portador save(Portador portador) {
		return this.portadorRepository.save(portador);
	}

	@Override
	public Optional<Portador> findByEmail(String email) {
	  log.info("Buscando portador pelo email {}", email);
      return Optional.ofNullable(this.portadorRepository.findByEmail(email));
	}

	@Override
	public Optional<Portador> findbyId(Long id) {
		return Optional.ofNullable(portadorRepository.findOne(id));
	}

	@Override
	public List<Portador> findAll() {
		return portadorRepository.findAll();
	}

	@Override
	public Optional<Portador> findByCpf(String cpf) {
		log.info("Buscando portador pelo cpf {}", cpf);
	      return Optional.ofNullable(this.portadorRepository.findByCpf(cpf));
	}

}
