package com.desafio.api.services.impl;

import com.desafio.api.entities.Usuario;
import com.desafio.api.repositories.UsuarioRepository;
import com.desafio.api.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Optional;

@Service
public class UsuarioServiceImpl implements UsuarioService {

    private static final Logger log = LoggerFactory.getLogger(UsuarioServiceImpl.class);

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Override
    public Usuario save(Usuario usuario) {
        log.info("Persistindo usuario: {}", usuario);
        return this.usuarioRepository.save(usuario);
    }

    @Override
    public Optional<Usuario> findByEmail(String email) {
        log.info("Buscando usuario pelo email {}", email);
        return Optional.ofNullable(this.usuarioRepository.findByEmail(email));
    }

    @Override
    public Optional<Usuario> findById(Long id) {
    	return Optional.ofNullable(usuarioRepository.findOne(id));
    }
}
