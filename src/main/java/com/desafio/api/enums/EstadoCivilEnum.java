package com.desafio.api.enums;

public enum EstadoCivilEnum {
    SOLTEIRO,
    VIUVO,
    CASADO,
    DIVORCIADO,
}
