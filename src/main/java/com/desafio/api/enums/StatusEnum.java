package com.desafio.api.enums;

public enum StatusEnum {
    APROVADO,
    NEGADO,
    PENDENTE,
}
