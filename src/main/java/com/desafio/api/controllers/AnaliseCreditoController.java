package com.desafio.api.controllers;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.desafio.api.dtos.AnaliseCreditoDto;
import com.desafio.api.entities.AnaliseCredito;
import com.desafio.api.enums.StatusEnum;
import com.desafio.api.response.Response;
import com.desafio.api.services.AnaliseCreditoService;
import com.desafio.api.services.PortadorService;

@RestController
@RequestMapping("/api/analise-credito")
@CrossOrigin(origins = "*")
public class AnaliseCreditoController {
	private static final Logger log = LoggerFactory.getLogger(UsuarioController.class);
	
	@Autowired
	private AnaliseCreditoService analiseCreditoService;
	
	@Autowired
	private PortadorService portadorService;
	
	@PutMapping
	@PreAuthorize("hasRole('ANALISTA')")
	public ResponseEntity<Response<AnaliseCreditoDto>> save(@Valid @RequestBody AnaliseCreditoDto analiseCreditoDto,
			BindingResult result) throws NoSuchAlgorithmException {
		log.info("Cadastrando Analise de crédito: {}", analiseCreditoDto.toString());
		Response<AnaliseCreditoDto> response = new Response<AnaliseCreditoDto>();

		AnaliseCredito analiseCredito = this.dtoToAnaliseCredito(analiseCreditoDto);
		
		if (result.hasErrors()) {
			log.error("Erro validando dados de cadastro analise de crédito: {}", result.getAllErrors());
			result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
			return ResponseEntity.badRequest().body(response);
		}
		this.analiseCreditoService.save(analiseCredito);
		
		response.setData(this.analiseCreditoToDto(analiseCredito));
		return ResponseEntity.ok(response);
	}
	
	@GetMapping
	public ResponseEntity<Response<List<AnaliseCredito>>> findAll() throws NoSuchAlgorithmException{
		Response<List<AnaliseCredito>> response = new Response<>();
		List<AnaliseCredito> data = this.analiseCreditoService.findAll();
		response.setData(data);
		return ResponseEntity.ok(response);
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<Response<AnaliseCredito>> findById(@PathVariable("id") Long id) throws NoSuchAlgorithmException{
		Response<AnaliseCredito> response = new Response<>();
		AnaliseCredito analise = this.analiseCreditoService.findbyId(id).get();
		response.setData(analise);
		return ResponseEntity.ok(response);
	}
	 
	private AnaliseCreditoDto analiseCreditoToDto(AnaliseCredito analiseCredito) {
		AnaliseCreditoDto analiseCreditoDto = new AnaliseCreditoDto();
		analiseCreditoDto.setId(analiseCredito.getId());
		analiseCreditoDto.setStatus(analiseCredito.getStatus().toString());
		analiseCreditoDto.setPortador(analiseCredito.getPortador());
		return analiseCreditoDto;
	}

	private AnaliseCredito dtoToAnaliseCredito(AnaliseCreditoDto analiseCreditoDto) {
		AnaliseCredito analise = analiseCreditoService.findbyId(analiseCreditoDto.getId()).get();
		AnaliseCredito analiseChanged = new AnaliseCredito();
		analiseChanged.setId(analiseCreditoDto.getId());
		analiseChanged.setDataAtualizacao(analise.getDataAtualizacao());
		analiseChanged.setDataCriacao(analise.getDataCriacao());
		analiseChanged.setStatus(StatusEnum.valueOf(analiseCreditoDto.getStatus()));
		analiseChanged.setPortador(portadorService.findbyId(analiseCreditoDto.getPortador().getId()).get());
		return analiseChanged;
	}
}
