package com.desafio.api.controllers;

import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.desafio.api.dtos.PortadorDto;
import com.desafio.api.entities.AnaliseCredito;
import com.desafio.api.entities.Portador;
import com.desafio.api.enums.EstadoCivilEnum;
import com.desafio.api.enums.SexoEnum;
import com.desafio.api.enums.StatusEnum;
import com.desafio.api.response.Response;
import com.desafio.api.services.AnaliseCreditoService;
import com.desafio.api.services.PortadorService;

@RestController
@RequestMapping("/api/portador")
@CrossOrigin(origins = "*")
public class PortadorController {

	private static final Logger log = LoggerFactory.getLogger(UsuarioController.class);
	
	@Autowired
	private PortadorService portadorService;
	
	@Autowired
	private AnaliseCreditoService analiseCreditoService;
	/**
	 * 
	 * @param portadorDto
	 * @param result
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	@PostMapping
	public ResponseEntity<Response<PortadorDto>> save(@Valid @RequestBody PortadorDto portadorDto,
			BindingResult result) throws NoSuchAlgorithmException {
		log.info("Cadastrando Portador: {}", portadorDto.toString());
		Response<PortadorDto> response = new Response<PortadorDto>();

		Portador portador = this.dtoToPortador(portadorDto);

		if (result.hasErrors()) {
			log.error("Erro validando dados de cadastro Portador: {}", result.getAllErrors());
			result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
			return ResponseEntity.badRequest().body(response);
		}

		

		this.portadorService.save(portador);
		response.setData(this.portadorToDto(portador));
		
		
		AnaliseCredito analiseCredito = new AnaliseCredito();
		analiseCredito.setPortador(portador);
		analiseCredito.setStatus(StatusEnum.PENDENTE);
		
		this.analiseCreditoService.save(analiseCredito);
		
		return ResponseEntity.ok(response);
	}
	
	/**
	 * Atualiza os dados de um portador.
	 *
	 * @param id
	 * @param portadorDto
	 * @param result
	 * @return ResponseEntity<Response<FuncionarioDto>>
	 * @throws NoSuchAlgorithmException
	 */
	@PutMapping(value = "/{id}")
	public ResponseEntity<Response<PortadorDto>> update(@PathVariable("id") Long id,
			@Valid @RequestBody PortadorDto portadorDto, BindingResult result) throws NoSuchAlgorithmException {
		Response<PortadorDto> response = new Response<PortadorDto>();

		log.info("Atualizando portador: {}", portadorDto.toString());
		
		portadorDto.setId(id);
		Optional<Portador> portador  = portadorService.findbyId(id);
		
		if (!portador.isPresent()) {
			result.addError(new ObjectError("portador", "Portador não encontrado. ID inexistente."));
		}
		if (result.hasErrors()) {
			log.error("Erro validando portador: {}", result.getAllErrors());
			result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
			return ResponseEntity.badRequest().body(response);
		}
		
		Portador changedPortador = this.dtoToPortador(portadorDto);
		changedPortador.setDataAtualizacao(portador.get().getDataAtualizacao());
		changedPortador.setDataCriacao(portador.get().getDataCriacao());
		
		response.setData(portadorToDto(this.portadorService.save(changedPortador)));
		return ResponseEntity.ok(response);
	}
	
	@GetMapping
	public ResponseEntity<Response<List<Portador>>> findAll(){
		Response<List<Portador>> response = new Response<>();
		List<Portador> data = this.portadorService.findAll();
		response.setData(data);
		return ResponseEntity.ok(response);
	}
	
	/**
	 * Retorna um DTO com os dados de um portador.
	 *
	 * @param portador
	 * @return usuarioDto
	 */
	private PortadorDto portadorToDto(Portador portador) {
		PortadorDto portadorDto = new PortadorDto();
		portadorDto.setNome(portador.getNome());
		portadorDto.setId(portador.getId());
		portadorDto.setEmail(portador.getEmail());
		portadorDto.setCpf(portador.getCpf());
		portadorDto.setEstado(portador.getEstado());
		portadorDto.setEstadoCivil(portador.getEstadoCivil().toString());
		portadorDto.setSexo(portador.getSexo().toString());
		portadorDto.setIdade(String.valueOf(portador.getIdade()));
		portadorDto.setRenda(portador.getRenda().toString());
		
		return portadorDto;
	}
	/**
	 * Retorna um Portador 
	 * @param portadorDto
	 * @return
	 */
	private Portador dtoToPortador(PortadorDto portadorDto) {
		Portador portador = new Portador();
		portador.setId(portadorDto.getId());
		portador.setNome(portadorDto.getNome());
		portador.setEmail(portadorDto.getEmail());
		portador.setCpf(portadorDto.getCpf());
		portador.setEstado(portadorDto.getEstado());
		portador.setEstadoCivil(EstadoCivilEnum.valueOf(portadorDto.getEstadoCivil()));
		portador.setSexo(SexoEnum.valueOf(portadorDto.getSexo()));
		portador.setIdade(Integer.parseInt(portadorDto.getIdade()));
		portador.setRenda(Double.parseDouble(portadorDto.getRenda()));
		return portador;
	}

}
