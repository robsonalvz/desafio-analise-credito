package com.desafio.api.controllers;

import java.security.NoSuchAlgorithmException;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.desafio.api.dtos.UsuarioDto;
import com.desafio.api.entities.Usuario;
import com.desafio.api.enums.PerfilEnum;
import com.desafio.api.response.Response;
import com.desafio.api.services.UsuarioService;
import com.desafio.api.utils.PasswordUtils;

@RestController
@RequestMapping("/api/usuario")
@CrossOrigin(origins = "*")
public class UsuarioController {
    private static final Logger log = LoggerFactory.getLogger(UsuarioController.class);

    @Autowired
    private UsuarioService usuarioService;
    
    @PostMapping
	public ResponseEntity<Response<UsuarioDto>> save(@Valid @RequestBody UsuarioDto usuarioDto,
			BindingResult result) throws NoSuchAlgorithmException {
		log.info("Cadastrando Usuário: {}", usuarioDto.toString());
		Response<UsuarioDto> response = new Response<UsuarioDto>();
		
		if (this.usuarioService.findByEmail(usuarioDto.getEmail()).isPresent()) {
			result.addError(new ObjectError("usuario", "Usuário ja cadastrado."));
		}
		
		Usuario usuario = new Usuario();
		
		usuario.setEmail(usuarioDto.getEmail());
		usuario.setSenha(PasswordUtils.gerarBCrypt(usuarioDto.getSenha()));
		usuario.setPerfil(PerfilEnum.ROLE_USUARIO);
		
		if (result.hasErrors()) {
			log.error("Erro validando dados de cadastro Usuário: {}", result.getAllErrors());
			result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
			return ResponseEntity.badRequest().body(response);
		}
		
		response.setData(this.userToDto(this.usuarioService.save(usuario)));
		return ResponseEntity.ok(response);
	}
    

	/**
     * Atualiza os dados de um usuário.
     *
     * @param id
     * @param usuarioDto
     * @param result
     * @return ResponseEntity<Response<FuncionarioDto>>
     * @throws NoSuchAlgorithmException
     */
    @PutMapping(value = "/{id}")
    public ResponseEntity<Response<UsuarioDto>> update(@PathVariable("id") Long id,
                                                          @Valid @RequestBody UsuarioDto usuarioDto, BindingResult result) throws NoSuchAlgorithmException {
        log.info("Atualizando usuario: {}", usuarioDto.toString());
        Response<UsuarioDto> response = new Response<UsuarioDto>();

        Optional<Usuario> usuario = this.usuarioService.findById(id);
        if (!usuario.isPresent()) {
            result.addError(new ObjectError("usuario", "Usuário não encontrado."));
        }

        this.updateUserData(usuario.get(), usuarioDto, result);

        if (result.hasErrors()) {
            log.error("Erro validando usuário: {}", result.getAllErrors());
            result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
            return ResponseEntity.badRequest().body(response);
        }

        this.usuarioService.save(usuario.get());
        response.setData(this.userToDto(usuario.get()));

        return ResponseEntity.ok(response);
    }
    /**
     * Atualiza os dados do usuário com base nos dados encontrados no DTO.
     *
     * @param usuario
     * @param usuarioDto
     * @param result
     * @throws NoSuchAlgorithmException
     */
    private void updateUserData(Usuario usuario, UsuarioDto usuarioDto, BindingResult result)
            throws NoSuchAlgorithmException {
    	
        if (!usuario.getEmail().equals(usuarioDto.getEmail())) {
            this.usuarioService.findByEmail(usuarioDto.getEmail())
                    .ifPresent(func -> result.addError(new ObjectError("email", "Email já existente.")));
            usuario.setEmail(usuarioDto.getEmail());
        }

        if (!usuarioDto.getSenha().isEmpty()) {
            usuario.setSenha(PasswordUtils.gerarBCrypt(usuarioDto.getSenha()));
        }
    }

    /**
     * Retorna um DTO com os dados de um usuario.
     *
     * @param usuario
     * @return usuarioDto
     */
    private UsuarioDto userToDto(Usuario usuario) {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setId(usuario.getId());
        usuarioDto.setEmail(usuario.getEmail());
        return usuarioDto;
    }

}


