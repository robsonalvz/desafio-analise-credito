package com.desafio.api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.desafio.api.entities.Portador;

@Transactional(readOnly = true)
public interface PortadorRepository extends JpaRepository<Portador, Long> {
	 Portador findByEmail(String email);
	 Portador findByCpf(String cpf);
}
