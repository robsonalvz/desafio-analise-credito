package com.desafio.api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.desafio.api.entities.AnaliseCredito;

@Transactional(readOnly = true)
public interface AnaliseCreditoRepository extends JpaRepository<AnaliseCredito, Long> {

}
