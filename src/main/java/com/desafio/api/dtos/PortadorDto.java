package com.desafio.api.dtos;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.br.CPF;

import lombok.Data;

@Data
public class PortadorDto {
	private Long id;
	
	@NotEmpty(message = "Nome não pode ser vazio.")
	@Length(min = 3, max = 200, message = "Nome deve conter entre 3 e 200 caracteres.")
	private String nome;
	
	@NotEmpty(message = "Sexo não pode ser vazio.")
	private String sexo;
	
	@NotEmpty(message = "Idade não pode ser vazio.")
	private String idade;
	
	@NotEmpty(message = "Email não pode ser vazio.")
    @Length(min = 5, max = 200, message = "Email deve conter entre 5 e 200 caracteres.")
    @Email(message="Email inválido.")
	private String email;
	
	@NotEmpty(message = "CPF não pode ser vazio.")
	@CPF(message="CPF inválido")
	private String cpf;
	
	@NotEmpty(message = "Renda não pode ser vazio.")
	private String renda;
	
	@NotEmpty(message = "Estado não pode ser vazio.")
	private String estado;
	
	@NotEmpty(message = "Estado Civil não pode ser vazio.")
	private String estadoCivil;
	
	
}
