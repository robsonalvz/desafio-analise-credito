package com.desafio.api.dtos;

import com.desafio.api.entities.Portador;

import lombok.Data;

@Data
public class AnaliseCreditoDto {
	private Long id;
	private String status;
	private Portador portador;

}
